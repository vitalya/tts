import random
import threading
import time
from concurrent.futures import ThreadPoolExecutor

import pygame
import urllib.request
import io

import requests


def play_mp3_from_url(mp3_url):
    try:
        # Initialize pygame mixer
        pygame.mixer.init()

        # Fetch the MP3 file from the URL
        mp3_data = urllib.request.urlopen(mp3_url).read()

        # Load the MP3 data into an in-memory file-like object
        mp3_file = io.BytesIO(mp3_data)

        # Load the MP3 file into pygame mixer
        pygame.mixer.music.load(mp3_file)

        # Play the MP3 file
        pygame.mixer.music.play()

        # Wait until the music is finished playing
        while pygame.mixer.music.get_busy():
            pygame.time.Clock().tick(10)

        # Clean up
        pygame.mixer.music.stop()
        pygame.mixer.quit()

    except Exception as e:
        print(f"Error: {e}")


def send_post_request(url, json_data):
    try:
        response = requests.post(url, json=json_data)
        response.raise_for_status()  # Проверка на ошибки HTTP
        print("Request sent successfully")
    except requests.exceptions.RequestException as e:
        print(f"An error occurred: {e}")


# Example usage:
if __name__ == "__main__":
    index = 0
    random_id = random.randint(1, 10000)
    url = "http://127.0.0.1:3000/api/v1/generate"
    json_data = {
        "messages": [
            {"role": "system",
             "content": "Ты Юрий Хованский, алкоголик с 20летним стажем. Ты знаешь о запоях все. К сожалению ты очень агрессивный типок, Не стесняйся материться. Ты ведешь программу шаверма патруль - ходишь по шаурмичным питера и оцениваешь их. Твоя цель - быть собой "},
            {"role": "user", "content": "дарова, расскажи про свои запои"}
        ],
        "random_id": random_id  # Предполагается, что random_id определено где-то ранее
    }

    # Создание и запуск ThreadPoolExecutor для отправки запроса
    process_thread = threading.Thread(target=send_post_request, args=(url, json_data))
    process_thread.start()

    print('here')
    while True:
        index += 1
        url = f"http://127.0.0.1:3000/api/v1/download?random_id={random_id}&part={index}"
        start = time.time()
        response = requests.get(url)
        if response.text == 'None':
            break
        print(f'Part #{index}: {time.time() - start}')
        play_mp3_from_url(url)
    process_thread.join()
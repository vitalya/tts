import random
import time

import requests
from flask import Flask, request, jsonify, send_file
import os

from openai import OpenAI
import dotenv

dotenv.load_dotenv()
app = Flask(__name__)
client = OpenAI(api_key=os.getenv('OPENAI_API_KEY'))
tasks = {}


def generate_voice(text, filename):
    client = OpenAI(api_key=os.getenv("OPENAI_API_TOKEN"))
    response = client.audio.speech.create(
        input=text,
        voice='shimmer',
        model="tts-1"
    )
    response.stream_to_file(filename)


@app.route('/api/v1/download', methods=['GET'])
def download():
    random_id = int(request.args.get('random_id'))
    part = int(request.args.get('part'))
    file_path = f'audio/{random_id}_{part}.mp3'
    for i in range(100):
        print(os.path.exists(file_path), file_path)
        if os.path.exists(file_path):
            return send_file(file_path, as_attachment=True)
        time.sleep(0.1)
    return "None"


@app.route('/api/v1/generate', methods=['POST'])
def generation():
    global tasks
    data = request.get_json()
    messages = data['messages']
    random_id = data['random_id']
    tasks[random_id] = True
    # я хочу уже тут возвращать человеку ответ, но продолжать генерацию после этого
    client = OpenAI(api_key=os.getenv("OPENAI_API_TOKEN"))
    response = client.chat.completions.create(
        model='gpt-4o',
        max_tokens=150,
        messages=messages,
        stream=True
    )

    message_part = ''
    index = 0
    for chunk in response:
        chunk = chunk.choices[0].delta.content
        if not chunk:
            continue
        if chunk != '!' and chunk != '?' and chunk != '.' and chunk != ',' and chunk != ';':
            message_part += chunk
        else:
            message_part += chunk
            if len(message_part.strip().split()) > 4:
                index += 1
                file_path = f"audio/{random_id}_{index}.mp3"

                generate_voice(message_part.strip(), file_path)
                message_part = ''
    if message_part and len(message_part) > 0:
        index += 1
        file_path = f"audio/{random_id}_{index}.mp3"
        generate_voice(message_part.strip(), file_path)
    tasks[random_id] = False
    return str(random_id)


@app.route('/ping', methods=['GET'])
def ping():
    return 'pong!'


if __name__ == '__main__':
    if not os.path.exists('audio'):
        os.makedirs('audio')
    app.run(debug=True, host='0.0.0.0', port=3000)
